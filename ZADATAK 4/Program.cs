﻿using System;
using System.Collections.Generic;

namespace ZADATAK_4
{ /* uocava se razlika u ispisu*/
    class Program
    {
        static void Main(string[] args)
        {
            Book rentedHotBook = new Book("Never ever");
            Video rentedHotVideo = new Video("Hunger Games 2 & 3");
            Book rentedBook = new Book("Eat, Love, Pray");
            Video rentedVideo = new Video("Hunger Games");
            HotItem hotBook = new HotItem(rentedHotBook);
            HotItem hotVideo = new HotItem(rentedHotVideo);
            List<IRentable> RentedStuff = new List<IRentable>();
            RentingConsolePrinter rentingPrinter = new RentingConsolePrinter();
            RentedStuff.Add(rentedBook);
            RentedStuff.Add(rentedVideo);
            RentedStuff.Add(hotBook);
            RentedStuff.Add(hotVideo);
            rentingPrinter.DisplayItems(RentedStuff);
            rentingPrinter.PrintTotalPrice(RentedStuff);
        }
    }
}
