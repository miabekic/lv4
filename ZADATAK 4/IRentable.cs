﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_4
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();

    }
}
