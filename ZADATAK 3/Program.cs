﻿using System;
using System.Collections.Generic;

namespace ZADATAK_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Book rentedBook = new Book("Eat, Love, Pray");
            Video rentedVideo = new Video("Hunger Games");
            List<IRentable> RentedStuff = new List<IRentable>();
            RentingConsolePrinter rentingPrinter = new RentingConsolePrinter();
            RentedStuff.Add(rentedBook);
            RentedStuff.Add(rentedVideo);
            rentingPrinter.DisplayItems(RentedStuff);
            rentingPrinter.PrintTotalPrice(RentedStuff);
            
        }
    }
}
