﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_7
{
    class EmailValidator:IEmailValidatorService
    {
        private bool ContainsATCharacterAT(String candidat)
        {
           return candidat.Contains('@');
        }
        private bool endWith(String candidat)
        {
            return candidat.EndsWith(".hr") || candidat.EndsWith(".com");
        }
        public bool IsValidAddress(String candidate)
        {
            return ContainsATCharacterAT(candidate) && endWith(candidate);   
        }

    }
}
