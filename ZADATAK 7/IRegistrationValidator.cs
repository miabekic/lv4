﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_7
{
    interface IRegistrationValidator
    {
        bool IsUserEntryValid(UserEntry entry);
    }
}
