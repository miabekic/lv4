﻿using System;

namespace ZADATAK_7
{
    class Program
    {
        static void Main(string[] args)
        {
            RegistrationValidator registrationValidator = new RegistrationValidator();
            UserEntry user = UserEntry.ReadUserFromConsole();
            while (registrationValidator.IsUserEntryValid(user) != true)
            {
                user = UserEntry.ReadUserFromConsole();
            }
        }
    }
}
