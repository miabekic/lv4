﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_7
{
    class RegistrationValidator:IRegistrationValidator
    {
        private EmailValidator emailValidator;
        private PasswordValidator passwordValidator;

        public RegistrationValidator()
        {
            emailValidator = new EmailValidator();
            passwordValidator = new PasswordValidator(8);
        }
        private bool isValidEmail(String email)
        {
            return emailValidator.IsValidAddress(email);
        }
        private bool isValidPassword(String password)
        {
            return passwordValidator.IsValidPassword(password);
        }
        public bool IsUserEntryValid(UserEntry entry)
        {
            return isValidEmail(entry.Email) && isValidPassword(entry.Password);
        }

    }
}
