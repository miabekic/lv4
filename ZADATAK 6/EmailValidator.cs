﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_6
{
    class EmailValidator:IEmailValidatorService
    {
        private bool ContainsCharacterAT(String candidat)
        {
           return candidat.Contains('@');
        }
        private bool endWith(String candidat)
        {
            return candidat.EndsWith(".hr") || candidat.EndsWith(".com");
        }
        public bool IsValidAddress(String candidate)
        {
            return ContainsCharacterAT(candidate) && endWith(candidate);
        }
        
    }
}
