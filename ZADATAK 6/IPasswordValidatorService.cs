﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_6
{
    interface IPasswordValidatorService
    {
        bool IsValidPassword(String candidate);
    }
}
