﻿using System;

namespace ZADATAK_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataSet = new Dataset(@"C:\Users\Mia\Desktop\Fakultet\RPPOON\LV4\ZADATAK 2\CSV.txt");
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer);
            double[] averagePerRow = adapter.CalculateAveragePerRow(dataSet);
            double[] averagePerColumn = adapter.CalculateAveragePerColumn(dataSet);
            Console.WriteLine("Rows Average: ");
            for (int i = 0; i < averagePerRow.Length; i++)
            {
                Console.WriteLine("On index " + i + " -> " + averagePerRow[i]/10); //podjeljeno s 10 jer mi zadani pars ne radi ispravno, zamijenski isto nije pomogao
            }
            Console.WriteLine("\nColumns Average: ");
            for (int i = 0; i < averagePerColumn.Length; i++)
            {
                Console.WriteLine("On index " + i + " -> " + averagePerColumn[i]/10);
            }
        }
    }
}
