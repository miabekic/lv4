﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_2
{
    class Adapter:IAnalytics
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }
        private double[][] ConvertData(Dataset dataset)
        {
            int rowCount = dataset.GetData().Count;
            int columnCount = dataset.GetData()[rowCount - 1].Count;
            double[][] convertData = new double[rowCount][];
            for (int i = 0; i < rowCount; i++)
            {
                convertData[i] = new double[columnCount];
                for (int j = 0; j < columnCount; j++)
                {
                    convertData[i][j] = dataset.GetData()[i][j];
                }
            }
            return convertData;
        }
            public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }
}
