﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_5
{
    class DiscountedItem : RentableDecorator
    {
        public readonly double Discount;
        public DiscountedItem(IRentable rentable, double discount) : base(rentable)
        {
            Discount = discount;
        }
        public override double CalculatePrice()
        {
            return base.CalculatePrice() - (base.CalculatePrice() * Discount) / 100;
        }
        public override String Description
        {
            get
            {
                return base.Description + " -> "+ "Now at" + " "+ Discount + "% off!“";
            }


        }
    }
}
