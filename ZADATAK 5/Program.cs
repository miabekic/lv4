﻿using System;
using System.Collections.Generic;

namespace ZADATAK_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Book rentedHotBook = new Book("Special");
            Video rentedHotVideo = new Video("Hunger Games 2 & 3");
            Book rentedBook = new Book("Eat, Love, Pray");
            Video rentedVideo = new Video("Hunger Games");
            HotItem hotBook = new HotItem(rentedHotBook);
            HotItem hotVideo = new HotItem(rentedHotVideo);
            List<IRentable> RentedStuff = new List<IRentable>();
            RentingConsolePrinter rentingPrinter = new RentingConsolePrinter();
            RentedStuff.Add(rentedBook);
            RentedStuff.Add(rentedVideo);
            RentedStuff.Add(hotBook);
            RentedStuff.Add(hotVideo);

            List<IRentable> flashSale = new List<IRentable>();
            foreach(IRentable rentable in RentedStuff)
            {
                DiscountedItem discountedItem = new DiscountedItem(rentable, 15);
                flashSale.Add(discountedItem);
            }
            rentingPrinter.DisplayItems(flashSale);
            rentingPrinter.PrintTotalPrice(flashSale);
        }
    }
}
